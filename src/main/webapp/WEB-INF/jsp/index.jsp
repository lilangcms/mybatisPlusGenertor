<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="org.nbpay.demo.utils.OrderGenerator"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>NBPAY-DEMO</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="common/bootstrap.min.css" rel="stylesheet" type="text/css">
    <% String refBizNo = OrderGenerator.getOrderCode(); %>
</head>
<body>
    <div class="container">
        <form id="toPayForm" action="pay" method="post" >
			
            <div class="row">
                <label id="lblHeader" class="control-label" style="font-weight:normal"></label>
				<input type="hidden" id="menrchantId" name="menrchantId" value="1" />
				<input type="hidden" id="refBizNo" name="refBizNo" value="<%= refBizNo%>" />
                <hr />
                <label id="lblMaintain" ></label>
            </div>
           
            <div id="divLog"  class="form-group">
            	<label id="Label11" class="control-label">商家订单号 ：<%= refBizNo%></label>
            	<br />
            	<label id="Label11" class="control-label">类型：</label>
            	<br />
            	<font size="1" face="arial" color="red">RMB类型为用户输入固定支付的RMB，具体价值多少usdt，由后台计算，手续费由商家承担（usdt转移时扣取）；USDT类型为用户需要购买固定数量USDT，具体多少RMB金额由后台计算，手续费直接体现在币价和实时汇率差价上</font>
            	<br />
				<label><input name="type" type="radio" value="1" />基于RMB </label> 
				<label><input name="type" type="radio" value="2" checked/>基于USDT </label>   
				<br />          	
                <!--<input id="type" name="type" type="text" class="form-control">-->            	
            	<label id="Label11" class="control-label">购买金额：</label>
            	<br />
            	<font size="1" face="arial" color="red">为了防风，RMB类型请输入100以上，USDT类型请输入20以上</font>
            	<br />
                <input id="amount" name="amount" type="text" class="form-control">
                <br />
                <button id="amount_submit" class="btn btn-primary" >提交</button>
            </div>

            
        </form>
    </div>
	<script src="common/jquery-3.1.1.min.js"></script>
    <script src="common/bootstrap.min.js"></script>
    <script src="common/jquery.timer.js"></script>
    <script type="text/javascript" >

    $(document).ready(function () {
	    $("#amount_submit").click(function () {
	    	$("form[name='toPayForm']").submit();
	    });
    });
    </script>
</body>
</html>
