<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ page import="org.nbpay.demo.*"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>NBPAY-DEMO-pay</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="common/bootstrap.min.css" rel="stylesheet" type="text/css">
</head>
<body>
    <div class="container">
        <form id="form1" onsubmit="return false;">
			
            <div class="row">
                <label id="lblHeader" class="control-label" style="font-weight:normal"></label>
                <hr />
                <label id="lblMaintain" ></label>
            </div>
           
            <div id="divLog"  class="form-group">
                <label id="Label10" class="control-label">商家订单号 ：${result.refBizNo}</label>
                <br />
            	<label id="Label10" class="control-label">平台支付单号 ：${result.orderBillNo}</label>
            	<br />
            	<label id="Label11" class="control-label">需要支付金额：${result.amount}</label>
                <br />
            	<label id="Label11" class="control-label">购买usdt数量：${result.coinAmount}</label>
                <br />
            	<label id="Label11" class="control-label">usdt购买价格：${result.price}</label>
                <br />   
				<a href="alipays://platformapi/startapp?appId=20000067&url=${encodeurl}">手机端唤起支付宝app支付</a>
                <br />                             
				<label id="Label11" class="control-label">pc端扫码支付</label>
                <br />
                <div id="payqrcode" class="col-xs-3" style="width: 300px;height: 300px;">
<!--                 	<img alt="支付二维码" src="${payurl}" align="middle" > -->
                </div>
            
            </div>
            <br/>
            <div>
                <button id="amount_submit" class="btn btn-primary" >用户确认已经支付</button>
                <button id="amount_error" class="btn btn-primary" >直接返回</button>
            </div>        
        </form>
    </div>
	<script src="common/jquery-3.1.1.min.js"></script>
    <script src="common/bootstrap.min.js"></script>
    <script src="common/jquery.timer.js"></script>
    <script type="text/javascript" src="https://cdn.bootcss.com/jquery.qrcode/1.0/jquery.qrcode.min.js"></script>
   	<script type="text/javascript">
   	function utf16to8(str) {
   	    var out, i, len, c;
   	    out = "";
   	    len = str.length;
   	    for (i = 0; i < len; i++) {
   	        c = str.charCodeAt(i);
   	        if ((c >= 0x0001) && (c <= 0x007F)) {
   	            out += str.charAt(i);
   	        } else if (c > 0x07FF) {
   	            out += String.fromCharCode(0xE0 | ((c >> 12) & 0x0F));
   	            out += String.fromCharCode(0x80 | ((c >> 6) & 0x3F));
   	            out += String.fromCharCode(0x80 | ((c >> 0) & 0x3F));
   	        } else {
   	            out += String.fromCharCode(0xC0 | ((c >> 6) & 0x1F));
   	            out += String.fromCharCode(0x80 | ((c >> 0) & 0x3F));
   	        }
   	    }
   	    return out;
   	}
   		jQuery('#payqrcode').qrcode(utf16to8("${payurl}"));
   	</script >
    
    <script type="text/javascript" >

    $(document).ready(function () {
	    $("#amount_submit").click(function () {
	    	alert("支付成功");
	    });
    });

    $("#amount_submit").click(function () {
        $.ajax({
               type: "get",
               url: "/userComfirm/${result.orderBillNo}/${result.refBizNo}",
              /*  data: { "method": 'userComfirm', "orderBillNo": , "refBizNo": }, */
              /*  dataType: "json", */
               async: false,
               success: function (result) {
                       window.location.href = "/";
               },
               error: function (xhr) {
               }
           });
   });
    
   $("#amount_error").click(function () {
       window.location.href = "/";
   });      
    </script>
</body>
</html>
