package com.lang.prod;

import java.io.File;
import java.util.List;

import com.lang.prod.tools.designPatterns.filter.chain.AuthenticationFilter;
import com.lang.prod.tools.designPatterns.filter.chain.DebugFilter;
import com.lang.prod.tools.designPatterns.filter.client.LangClient;
import com.lang.prod.tools.designPatterns.filter.manager.FilterManager;
import com.lang.prod.tools.designPatterns.filter.target.Target;
import com.lang.prod.tools.file.FileSearchs;
import com.lang.prod.tools.string.StringUtils;
import com.lang.prod.tools.string.WordCutUtils;

/**
 * @author lang.li E-mail:
 * @version 创建时间：2020年9月21日 上午11:43:08 Test.java
 */
public class Test {
	public static void test1() {
		FilterManager filterManager = new FilterManager(new Target());
		filterManager.setFilter(new AuthenticationFilter());
		filterManager.setFilter(new DebugFilter());

		LangClient client = new LangClient();
		client.setFilterManager(filterManager);
		client.sendRequest("home");
	}

	public static void test2() {
		List<File> files = FileSearchs.searchContent(new File("D:\\work\\files\\linux"), "firewall-cmd");
		System.out.println("共找到:" + files.size() + "个文件");
		for (File file : files) {
			System.out.println(file.getAbsolutePath());
		}
	}

	public static void test32() {
		String text = "邓青（500113198601112828，15902392811）于2021年4月10日18点左右，在云南省昆明时，通过抖音认识了一名男子，对方自称叫“付成豪”，在重庆解放碑汇丰银行工作，之后双方互相加了QQ和微信。最初，邓青与对方都是聊感情方面的事情。4月17日，付成豪称因自己是银行员工，在从事外汇交易，有内幕消息，可以稳赚不赔。当时付成豪发了一个链接给邓青，并告诉了邓青的账号和密码。邓青通过链接下载了一个名为“嘉盛集团”的APP，并登陆付成豪的账号发现付成豪的账户有盈利。邓青信以为真，于是在该平台上注册后，也进行了充值投资。邓青在付成豪的指导下进行投资后，平台账户上显示有盈利，但是不能提现，平台客服先以要等到预留期（2021年4月27日）后才能提现。2021年4月27日，邓青待预留期到了之后再次提现时，依然不能提现，客服要求邓青缴纳账户余额的15%作为税费，方能提现。邓青发现被骗，于是今日到所报警，共被骗19万元。作案时间：2021年4月20日14点59分至2021年4月25日16点21分。作案方式：杀猪盘。转款方式：银行卡转款。被害人账户：建设银行，卡号6227003763260414753，户名邓青。嫌疑账户：工商银行，卡号6212261408029528464，户名赖文宣；农业银行，卡号6230520680077306972，户名赖志森；中国邮政储蓄银行，卡号6221804310001401048，户名颜桂生；建设银行，卡号6217003230054222231，户名刘双武；工商银行，卡号6222081404001189026，户名黄远华；交通银行，卡号6222623570000434095，户名黄远华。嫌疑QQ号：912837638。嫌疑微信号：f15944068153。嫌疑平台：“嘉盛集团”，网址为www.jsjt0001.com。转款地点：重庆市巴南区龙洲湾金色田园小区4栋1单元5－3。。";
		System.out.println("---------0-----------------");
		StringUtils.getBetweenString(text, StringUtils.IDCARD).forEach(e->{System.out.println(e);});
		// 分词 转成string
		System.out.println(WordCutUtils.wordCutToString(text).toString());
		System.out.println("---------1-----------------");

		// 分词 ->得到数据
		System.out.println(WordCutUtils.wordCut(text).toString());
		System.out.println("---------2-----------------");

		// 词性标注
		System.out.println(WordCutUtils.wordPos(text).toString());
		System.out.println("---------3-----------------");

		// 实体识别
		System.out.println(WordCutUtils.wordNer(text).toString());
		System.out.println("---------4-----------------");

		// 分词，词性，实体识别
		System.out.println(WordCutUtils.wordAnalysis(text).toString());
	}
	
	public static void test4() {
		Integer a = 1;

		Integer b = 1;

		Integer c = 2000;

		Integer d = 2000;

		System.out.println(a==b);

		System.out.println(c==d);
	}

	public static void main(String[] args) {
		test4();
	}
}
