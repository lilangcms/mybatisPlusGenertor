package com.lang.prod.config;

import lombok.Data;

@Data
public class AliyuncsConfig {
	  private String accessKeyId;
	  private String accessKeySecret;
	  private String signName;
	  private String templateCode;
	  private String number;
}
