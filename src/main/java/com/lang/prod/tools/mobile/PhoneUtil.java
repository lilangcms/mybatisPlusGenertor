package com.lang.prod.tools.mobile;
import java.util.Locale;

import com.google.i18n.phonenumbers.PhoneNumberToCarrierMapper;
import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber.PhoneNumber;
import com.google.i18n.phonenumbers.geocoding.PhoneNumberOfflineGeocoder;
import com.lang.prod.tools.string.StringUtils;
/**
 * 号码归属地
 * @author lilang
 *
 */
public class PhoneUtil {
	
	
	private static PhoneNumberUtil phoneNumberUtil = PhoneNumberUtil.getInstance();
	 
    private static PhoneNumberToCarrierMapper carrierMapper = PhoneNumberToCarrierMapper.getInstance();
 
    private static PhoneNumberOfflineGeocoder geocoder = PhoneNumberOfflineGeocoder.getInstance();
 
    /**
     * 根据国家代码和手机号  判断手机号是否有效
     * @param phoneNumber
     * @param countryCode
     * @return
     */
    public static boolean checkPhoneNumber(String phoneNumber, String countryCode){
        int ccode = StringUtils.obj2Int(countryCode);
        long phone = StringUtils.toLong(phoneNumber);
        PhoneNumber pn = new PhoneNumber();
        pn.setCountryCode(ccode);
        pn.setNationalNumber(phone);
        return phoneNumberUtil.isValidNumber(pn);
 
    }
 
    /**
     * 根据国家代码和手机号  判断手机运营商
     * @param phoneNumber
     * @param countryCode
     * @return
     */
    public static String getCarrier(String phoneNumber, String countryCode){
        int ccode = StringUtils.obj2Int(countryCode);
        long phone = StringUtils.toLong(phoneNumber);
        PhoneNumber pn = new PhoneNumber();
        pn.setCountryCode(ccode);
        pn.setNationalNumber(phone);
        //返回结果只有英文，自己转成成中文
        String carrierEn = carrierMapper.getNameForNumber(pn, Locale.ENGLISH);
        String carrierZh = "";
        carrierZh += geocoder.getDescriptionForNumber(pn, Locale.CHINESE);
        switch (carrierEn) {
        case "China Mobile":
            carrierZh += " 中国移动";
            break;
        case "China Unicom":
            carrierZh += " 中国联通";
            break;
        case "China Telecom":
            carrierZh += " 中国电信";
            break;
        default:
            break;
        }
        return carrierZh;
    }
 
 
    /**
     * 
    * @Description: 根据国家代码和手机号  手机归属地
    * @param @param phoneNumber
    * @param @param countryCode
    * @param @return    参数
    * @throws
     */
    public static String getGeo(String phoneNumber, String countryCode){
        int ccode = StringUtils.obj2Int(countryCode);
        long phone = StringUtils.toLong(phoneNumber);
        PhoneNumber pn = new PhoneNumber();
        pn.setCountryCode(ccode);
        pn.setNationalNumber(phone);
        return geocoder.getDescriptionForNumber(pn, Locale.CHINESE);
    }
    
    
    /**
     * 
      * @Title: getPhoneRegionCode
      * @Description: 得到手机的归宿地编码
      * @return String    返回类型
      * @throws
     */
    public static String getPhoneRegionCode(String phoneNumber, String countryCode){
    	String areaName=getGeo(phoneNumber,countryCode);
    	if(StringUtils.isEmpty(areaName)){
    		return "";
    	}
    	if(areaName.length()<3){
    		return "";
    	}
    	return areaName;
    }
 
    
    public static void main(String[] args) {
        System.out.println(getCarrier("15696423059","86"));
    }
}
