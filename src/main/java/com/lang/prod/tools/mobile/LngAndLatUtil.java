package com.lang.prod.tools.mobile;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.HashMap;
import java.util.Map;

import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Maps;

public class LngAndLatUtil {
	public static final String PHONE_TO_CITY_TAOBAO="http://tcc.taobao.com/cc/json/mobile_tel_segment.htm?tel=";
    public static final String PHONE_TO_CITY_HAO_SERVICE="http://apis.haoservice.com/mobile";
    public static final String PHONE_TO_CITY_PAIPAI="http://virtual.paipai.com/extinfo/GetMobileProductInfo?amount=10000&mobile=";
    public static final String CITY_TO_LAT_LON_BAIDU="http://api.map.baidu.com/geocoder/v2/?output=json";
    public static final String BAIDU_AK = "xxxxxxx";
	public static Map<String, String> getCityNameByPhone(String phone)throws Exception
    {
        Map<String, String> map = new HashMap<String, String>();
        String url = PHONE_TO_CITY_PAIPAI + phone;
        String json = loadJSON(url);
        json=json.replace("(", "").replace(")", "").replace(";", "").substring(0, json.indexOf("<")-3);
        JSONObject obj = JSONObject.parseObject(json);
 
        {
            String cityName = obj.getString("cityname");
            String province = obj.getString("province");
            map.put("cityName", cityName);
            map.put("province", province);
        }
       
        return map;
    }
    
    public static Map<String,String> getLngLatByPhone(String phone)throws Exception{
        String city=getCityNameByPhone(phone).get("cityName");
        
        String url = CITY_TO_LAT_LON_BAIDU + "&ak="+BAIDU_AK+"&city="+city+"&address="+city;
        String json = loadJSON(url);
        
        JSONObject obj = JSONObject.parseObject(json);
        JSONObject location=obj.getJSONObject("result").getJSONObject("location");
        Map<String,String> latLng=Maps.newHashMap();
        latLng.put("lat", location.getString("lat"));
        latLng.put("lng", location.getString("lng"));
        return latLng;
    }
 
    private static String loadJSON(String url)throws Exception
    {
        StringBuilder json = new StringBuilder();
        try
        {
            URL oracle = new URL(url);
            URLConnection urlConn = oracle.openConnection();
            BufferedReader in = new BufferedReader(new InputStreamReader(urlConn.getInputStream(),"GBK"));
            String inputLine = null;
            while ((inputLine = in.readLine()) != null)
            {
                json.append(new String(inputLine));
            }
            in.close();
        }
        catch (IOException e)
        {
            throw e;
        }
        return json.toString();
    }
    
    
    
    public static void main(String[] args)throws Exception
    {
        System.out.println(getLngLatByPhone("18868716531").get("lat"));
    }
}
