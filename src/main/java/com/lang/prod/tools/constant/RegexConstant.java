package com.lang.prod.tools.constant;

public class RegexConstant {
	//匹配Email
    public static final int E_MAIL=0;
    //匹配手机
    public static final int MOBIL_PHONE=1;
    //匹配电话座机
    public static final int TEL_PHONE=2;
    //匹配邮编
    public static final int ZIP_CODE=3;
    //匹配URL
    public static final int URL=4;
    //匹配ip格式
    public static final int IP=5;
    //匹配HTML标签
    public static final int HTML_TAG=6;
    //匹配十六进制
    public static final int HEX=7;
    //匹配QQ号
    public static final int QQ=8;
    //匹配身份证号
    public static final int ID_CARD=9;
    //匹配正整数
    public static final int POSITIVE_INTEGER=10;
}
