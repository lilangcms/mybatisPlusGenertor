package com.lang.prod.tools.pdf;


import com.spire.doc.Document;
import com.spire.doc.FileFormat;

public class DocToPdfUtils {
	
	public static void doc2Pdf(String inFile,String outFile) {
		 //加载Word示例文档
        Document document = new Document();
        document.loadFromFile(inFile);
        //File file = new File(outFile);
        //FileOutputStream outputStream = new FileOutputStream(file);
        //保存结果文档
        //document.saveToFile(outputStream, FileFormat.PDF);
        document.saveToFile(outFile,FileFormat.PDF);
	}
	
	public static void main(String[] args) {
		doc2Pdf("D:/FTP/1.doc", "D:/FTP/1.pdf");
	}

}
