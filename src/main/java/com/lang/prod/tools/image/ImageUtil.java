package com.lang.prod.tools.image;

import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.Transparency;
import java.awt.geom.Ellipse2D;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

import javax.imageio.ImageIO;
import javax.imageio.stream.ImageOutputStream;
import cn.hutool.core.lang.Console;

public class ImageUtil {
    static final String IMAGE_SUFFIX = ".png";
    static final int HEAD_URL_WIDTH = 100;
    static final int HEAD_URL_HEIGHT = 100;

    /**
     * @param posterImgUrl     海报
     * @param tempQrCodeImgUrl 临时二维码
     * @param headImgUrl       头像
     * @return 合成图片地址
     */
    public static String drawImage(String posterImgUrl, String tempQrCodeImgUrl, String headImgUrl) throws IOException {

        BufferedImage posterBufImage = ImageIO.read(new URL(posterImgUrl));
        Graphics2D posterBufImageGraphics = posterBufImage.createGraphics();

        BufferedImage qrCodeImage = ImageIO.read(new URL(tempQrCodeImgUrl));
        BufferedImage headImage = ImageIO.read(new URL(headImgUrl));

        //设置圆形头像
        BufferedImage roundHeadImg = new BufferedImage(headImage.getWidth(), headImage.getHeight(), BufferedImage.TYPE_INT_RGB);

        Graphics2D roundHeadGraphics = roundHeadImg.createGraphics();
        Ellipse2D.Double shape = new Ellipse2D.Double(0, 0, roundHeadImg.getWidth(), roundHeadImg.getHeight());
        roundHeadGraphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        roundHeadImg = roundHeadGraphics.getDeviceConfiguration().createCompatibleImage(headImage.getWidth(), headImage.getHeight(),
                Transparency.TRANSLUCENT);
        roundHeadGraphics = roundHeadImg.createGraphics();
        // 使用 setRenderingHint 设置抗锯齿
        roundHeadGraphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        roundHeadGraphics.setClip(shape);
        roundHeadGraphics.drawImage(headImage, 0, 0, null);
        roundHeadGraphics.dispose();

        posterBufImageGraphics.drawImage(qrCodeImage, (posterBufImage.getWidth() - qrCodeImage.getWidth()), 10, qrCodeImage.getWidth(), qrCodeImage.getHeight(), null);
        posterBufImageGraphics.drawImage(roundHeadImg, 50, 100, HEAD_URL_WIDTH, HEAD_URL_HEIGHT, null);
        posterBufImageGraphics.dispose();

        ByteArrayOutputStream bs = new ByteArrayOutputStream();
        ImageOutputStream imgOut = ImageIO.createImageOutputStream(bs);
        ImageIO.write(posterBufImage, "png", imgOut);
        InputStream inSteam = new ByteArrayInputStream(bs.toByteArray());
        //String url = OSSFactory.build().uploadSuffix(inSteam, IMAGE_SUFFIX);
        return "";//返回合成的图片地址url
    }


    public static void drawImage() throws IOException {
        //海报图
        String backgroundImageUrl = "http://img1.juimg.com/171010/330841-1G01000050879.jpg";
        //二维码
        String qrCodeImageUrl = "https://gimg2.baidu.com/image_search/src=http%3A%2F%2Fimgo.yqdown.com%2Fimg2021%2F2%2F19%2F21%2F20210219769767455209.jpg&refer=http%3A%2F%2Fimgo.yqdown.com&app=2002&size=f9999,10000&q=a80&n=0&g=0n&fmt=jpeg?sec=1619168773&t=e2cd76b5c8eff874c4fc3d8a75c774fb";
        //头像
        String headUrl = "http://pic.51yuansu.com/pic3/cover/00/63/25/589bdedf5475d_610.jpg";

        BufferedImage bgBufImage = ImageIO.read(new URL(backgroundImageUrl));
        Graphics2D bgBufImageGraphics = bgBufImage.createGraphics();

        BufferedImage qrCodeImage = ImageIO.read(new URL(qrCodeImageUrl));
        BufferedImage headImage = ImageIO.read(new URL(headUrl));
        //设置圆形图片
        BufferedImage roundHeadImg = new BufferedImage(headImage.getWidth(), headImage.getHeight(), BufferedImage.TYPE_INT_RGB);

        Graphics2D roundHeadGraphics = roundHeadImg.createGraphics();
        Ellipse2D.Double shape = new Ellipse2D.Double(0, 0, roundHeadImg.getWidth(), roundHeadImg.getHeight());
        roundHeadGraphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        roundHeadImg = roundHeadGraphics.getDeviceConfiguration().createCompatibleImage(headImage.getWidth(), headImage.getHeight(),
                Transparency.TRANSLUCENT);
        roundHeadGraphics = roundHeadImg.createGraphics();
        // 使用 setRenderingHint 设置抗锯齿
        roundHeadGraphics.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        roundHeadGraphics.setClip(shape);
        roundHeadGraphics.drawImage(headImage, 0, 0, null);
        roundHeadGraphics.dispose();

        bgBufImageGraphics.drawImage(qrCodeImage, (bgBufImage.getWidth() - qrCodeImage.getWidth()), 10, qrCodeImage.getWidth(), qrCodeImage.getHeight(), null);
        bgBufImageGraphics.drawImage(roundHeadImg, 50, 100, HEAD_URL_WIDTH, HEAD_URL_HEIGHT, null);
        bgBufImageGraphics.dispose();
        ImageIO.write(bgBufImage, "png", new File("D:\\demo1.png"));
        Console.log("生成图片完成");

    }


    public static void main(String[] args) throws IOException {
        /**
    	//海报图
        String backgroundImageUrl = "http://img1.juimg.com/171010/330841-1G01000050879.jpg";
        //二维码
        String qrCodeImageUrl = "https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1553504957130&di=02fae20a5c0f885d52299b2b1d682c86&imgtype=0&src=http%3A%2F%2Fimg.atobo.com%2FProductImg%2FEWM%2FUWeb%2F3%2F2%2F1%2F3%2F061%2F3213061%2F1.gif";
        //头像
        String headUrl = "http://pic.51yuansu.com/pic3/cover/00/63/25/589bdedf5475d_610.jpg";
        String url = ImageUtil.drawImage(backgroundImageUrl, qrCodeImageUrl, headUrl);

        Console.log("url={}",url);
        */
    	ImageUtil.drawImage();
    	
    	
    	
    }

}