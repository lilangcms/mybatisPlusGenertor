package com.lang.prod.tools.image;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;

import javax.imageio.ImageIO;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.BinaryBitmap;
import com.google.zxing.DecodeHintType;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.NotFoundException;
import com.google.zxing.Result;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.common.HybridBinarizer;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;

public class ImageReader {
	
	
	public String readImagePath(String path) throws IOException, NotFoundException {
		MultiFormatReader formatReader = new MultiFormatReader();
        File file = new File(path);

        BufferedImage image = ImageIO.read(file);
        BinaryBitmap binaryBitmap = new BinaryBitmap(new HybridBinarizer(new BufferedImageLuminanceSource(image)));

        //定义二维码的参数
        HashMap<DecodeHintType,Object> hints = new HashMap<>();
        hints.put(DecodeHintType.CHARACTER_SET, "utf-8");
        hints.put(DecodeHintType.TRY_HARDER, Boolean.TRUE);
        hints.put(DecodeHintType.PURE_BARCODE, Boolean.TRUE);

        Result result = formatReader.decode(binaryBitmap, hints);

        System.out.println("二维码解析结果：" + result.toString());
        System.out.println("二维码的格式：" + result.getBarcodeFormat());
        System.out.println("二维码的文本内容：" + result.getText());

        return "";
	}
	
	public void createImagePath() {
		System.out.println(1234);
        final int width = 300;
        final int height = 300;
        final String format = "png";
        final String content = "我爱你，中国!!!";
        //定义二维码的参数
        HashMap<EncodeHintType,Object> hints = new HashMap<>();
        hints.put(EncodeHintType.CHARACTER_SET, "utf-8");
        hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.M);
        hints.put(EncodeHintType.MARGIN, 2);
        //生成二维码
        try{
            BitMatrix bitMatrix = new MultiFormatWriter().encode(content, BarcodeFormat.QR_CODE, width, height, hints);
            File file = new File("D:/FTP/img/13.png");
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            MatrixToImageWriter.writeToStream(bitMatrix, format, fileOutputStream);
        }catch(Exception e){
            e.printStackTrace();
        }
	}
	
	public static void main(String[] args) throws NotFoundException, IOException {
		//new ImageReader().createImagePath();
		new ImageReader().readImagePath("D:/FTP/img/app.png");
	}
	

}
