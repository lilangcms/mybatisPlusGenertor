package com.lang.prod.tools.ocr;

import java.io.File;
import net.sourceforge.tess4j.ITesseract;
import net.sourceforge.tess4j.Tesseract;
import net.sourceforge.tess4j.TesseractException;

public class OcrUtil {
	
	public String readChisimImage(String path) {
		 //创建tess对象
        ITesseract instance = new Tesseract();
        //设置训练文件目录
        instance.setDatapath("tessdata");
        instance.setTessVariable("user_defined_dpi", "300");
        //设置训练语言
        instance.setLanguage("chi_sim");
        //执行转换
        try {
        	 //加载待读取图片
            File imageFile = new File(path);
			String result = instance.doOCR(imageFile);
			return result;
		} catch (TesseractException e) {
			e.printStackTrace();
			return null;
		}   
	}
	
	public String readEngImage(String path) {
		 //创建tess对象
       ITesseract instance = new Tesseract();
       //设置训练文件目录
       instance.setDatapath("tessdata");
       instance.setTessVariable("user_defined_dpi", "300");
       //设置训练语言
       instance.setLanguage("eng");
       //执行转换
       try {
       	 //加载待读取图片
           File imageFile = new File(path);
			String result = instance.doOCR(imageFile);
			return result;
		} catch (TesseractException e) {
			e.printStackTrace();
			return null;
		}   
	}
	
	public static void main(String[] args) {
		OcrUtil util  = new OcrUtil();
		String text = util.readChisimImage("D:/FTP/img/12.jpg");
		System.out.println(text);
	}

}
