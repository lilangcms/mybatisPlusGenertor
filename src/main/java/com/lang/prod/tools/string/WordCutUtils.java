package com.lang.prod.tools.string;

import java.util.ArrayList;
import java.util.List;

import me.midday.FoolNLTK;
import me.midday.lexical.AnalysisResult;
import me.midday.lexical.Entity;
import me.midday.lexical.LexicalAnalyzer;
import me.midday.lexical.Word;

/**
 * @Description: 分词处理工具
 */
public class WordCutUtils {

    /**
     * 获取分词数组
     * @param text
     * @return
     */
    public static List<String> wordCutToString(String text){
        List<String> str = new ArrayList<>();
        List<List<Word>> words = wordCut(text);
        words.forEach((ws)-> ws.forEach((w)-> str.add(w.toString())));
        return str;
    }

    /**
     * 分词
     * @param text
     * @return
     */
    public static List<List<Word>> wordCut(String text){
        LexicalAnalyzer lexicalAnalyzer = FoolNLTK.getLSTMLexicalAnalyzer();
        return lexicalAnalyzer.cut(text);
    }


    /**
     * 词性标注
     * @param text
     * @return
     */
    public static List<List<Word>> wordPos(String text){
        LexicalAnalyzer lexicalAnalyzer = FoolNLTK.getLSTMLexicalAnalyzer();
        return lexicalAnalyzer.pos(text);
    }

    /**
     * 实体识别
     * @param text
     * @return
     */
    public static List<List<Entity>> wordNer(String text){
        LexicalAnalyzer lexicalAnalyzer = FoolNLTK.getLSTMLexicalAnalyzer();
        return lexicalAnalyzer.ner(text);
    }


    /**
     *  分词，词性，实体识别
     * @param text
     * @return
     */
    public static List<AnalysisResult> wordAnalysis(String text){
        LexicalAnalyzer lexicalAnalyzer = FoolNLTK.getLSTMLexicalAnalyzer();
        return lexicalAnalyzer.analysis(text);
    }

}