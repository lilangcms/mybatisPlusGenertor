package com.lang.prod.tools.string;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringUtils {
	/**
	 * 宽断言	
	 *  (?=exp)	匹配exp前面的位置
		(?<=exp)	匹配exp后面的位置
		(?!exp)	匹配后面跟的不是exp的位置
		(?<!exp)	匹配前面不是exp的位置
	 */
	
	public static final String between_comma="(?<=\\“)(.+?)(?=\\”)";
	public static final String between_small_parentheses="(?<=\\()(.+?)(?=\\))";
	public static final String between_parentheses="(?<=\\（)(.+?)(?=\\）)";
	
	public static final String IDCARD = "(?<!\\d)^[1-9]\\d{5}(18|19|2([0-9]))\\d{2}(0[0-9]|10|11|12)([0-2][1-9]|30|31)\\d{3}[0-9Xx]$(?!\\d)";

	public static boolean equals(String str1, String str2) {
		return str1 == null ? str2 == null : str1.equals(str2);
	}

	/** * @param str * @return */
	public static boolean isEmpty(String str) {
		if (null == str || "".equals(str) || "null".equals(str)) {
			return true;
		}
		return false;
	}

	public static boolean isEmptyObject(Object obj) {
		if (null == obj || "".equals(obj)) {
			return true;
		}
		return false;
	}

	/** * @param str * @return */
	public static boolean isNotEmpty(String str) {
		return !isEmpty(str);
	}

	/**
	 * * @Title: obj2Long * @Description: 对象为null时候，转换对象 * @param: obj 对象 * @return:
	 * String 返回类型 * @author: rysun * @throws:
	 */
	public static long obj2Long(Object obj) {
		return obj == null ? 0l : Long.parseLong(obj.toString());
	}
	
	public static long toLong(Object obj) {
		return obj == null ? 0l : Long.parseLong(obj.toString());
	}
	
	public static int obj2Int(Object obj) {
		return obj == null ? 0 : Integer.parseInt(obj.toString());
	}
	
	
	public static List<String> getBigBetweenComma(String managers){
        List<String> ls=new ArrayList<String>();
        Pattern pattern = Pattern.compile("(?<=\\“)(.+?)(?=\\”)");
        Matcher matcher = pattern.matcher(managers);
        while(matcher.find())
            ls.add(matcher.group());
        return ls;
    }
	
	public static List<String> getBetweenString(String managers,String regx){
        List<String> ls=new ArrayList<String>();
        Pattern pattern = Pattern.compile(regx);
        Matcher matcher = pattern.matcher(managers);
        while(matcher.find())
            ls.add(matcher.group());
        return ls;
    }


	
	public static List<String> getBigParentheses(String managers){
        List<String> ls=new ArrayList<String>();
        Pattern pattern = Pattern.compile("(?<=\\（)(.+?)(?=\\）)");
        Matcher matcher = pattern.matcher(managers);
        while(matcher.find())
            ls.add(matcher.group());
        return ls;
    }
}