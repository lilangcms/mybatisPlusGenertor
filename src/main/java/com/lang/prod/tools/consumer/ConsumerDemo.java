package com.lang.prod.tools.consumer;

import java.util.function.Consumer;
import java.util.function.Supplier;

import com.lang.prod.domain.Goods;

public class ConsumerDemo {
	
	public void luckDay() {
        Supplier<String> girlWish = () -> "美女";
        Supplier<String> moneyWish = () -> "钱";
 
        String girl = magicLamp(girlWish);
        String money = magicLamp(moneyWish);
 
        System.out.println(girl + "---" + money);
    }
 
    //你想要什么神灯就给你什么
    public String magicLamp(Supplier<String> wish) {
        return wish.get();
    }
    
	public void cutHand() {
        Goods goods = new Goods("口红", 288);
 
        //土豪
        spentMoney(goods, (g) -> System.out.println("消费" + g.getPrice() + "元"));
 
        System.out.println("-------------------贫富分割线--------------------");
 
        //屌丝
        spentMoneyAndLog(goods, (g) -> System.out.println("消费" + g.getPrice() + "元"));
    }
    
    //任性地花
    public void spentMoney(Goods goods, Consumer<Goods> consumer) {
        consumer.accept(goods);
    }
 
    //花一笔记一笔
    public void spentMoneyAndLog(Goods goods, Consumer<Goods> consumer) {
        Consumer<Goods> logConsumer = (g) -> System.out.println("买" + g.getName() + "用了" + g.getPrice() + "元！");
        consumer.andThen(logConsumer).accept(goods);
    }
    public static void main(String[] args) {
		new ConsumerDemo().cutHand();
	}
}
