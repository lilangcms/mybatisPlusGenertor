package com.lang.prod.tools.regex;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

public class RegexGetPhone {

	/**
	 * ^ 匹配输入字符串开始的位置 \d 匹配一个或多个数字，其中 \ 要转义，所以是 \\d $ 匹配输入字符串结尾的位置
	 */
	private static final Pattern HK_PATTERN = Pattern.compile("^(5|6|8|9)\\d{7}$");
	private static final Pattern CHINA_PATTERN = Pattern.compile("^((13[0-9])|(14[0,1,4-9])|(15[0-3,5-9])|(16[2,5,6,7])|(17[0-8])|(18[0-9])|(19[0-3,5-9]))\\d{8}$");
	private static final Pattern NUM_PATTERN = Pattern.compile("[0-9]+");
	private static final Pattern ID_CARD = Pattern.compile("^[1-9]\\d{5}[1-9]\\d{3}((0\\d)|(1[0-2]))(([0|1|2]\\d)|3[0-1])\\d{3}([\\d|x|X]{1})$");
	/**
	 * 大陆号码或香港号码均可
	 */
	public static boolean isPhoneLegal(String str) throws PatternSyntaxException {
		return isChinaPhoneLegal(str) || isHKPhoneLegal(str);
	}

	/**
	 * 大陆手机号码11位数，匹配格式：前三位固定格式+后8位任意数 此方法中前三位格式有： 13+任意数 145,147,149
	 * 15+除4的任意数(不要写^4，这样的话字母也会被认为是正确的) 166 17+3,5,6,7,8 18+任意数 198,199
	 */
	public static boolean isChinaPhoneLegal(String str) throws PatternSyntaxException {
		Matcher m = CHINA_PATTERN.matcher(str);
		return m.matches();
	}

	/**
	 * 香港手机号码8位数，5|6|8|9开头+7位任意数
	 */
	public static boolean isHKPhoneLegal(String str) throws PatternSyntaxException {

		Matcher m = HK_PATTERN.matcher(str);
		return m.matches();
	}

	/**
	 * 判断是否是正整数的方法
	 */
	public static boolean isNumeric(String string) {
		return NUM_PATTERN.matcher(string).matches();
	}

	/**
	 * 查询符合的固定电话
	 * 
	 * @param str
	 */
	public static List<String> getCellphone(String str) {
		List<String> list = new ArrayList<>();
		// 将给定的正则表达式编译到模式中
		// 创建匹配给定输入与此模式的匹配器。
		Matcher matcher = CHINA_PATTERN.matcher(str);
		// 查找字符串中是否有符合的子字符串
		while (matcher.find()) {
			// 查找到符合的即输出
			list.add(matcher.group());
		}
		return list;
	}
	
	/**
	 * 查询符合的固定电话
	 * 
	 * @param str
	 */
	public static List<String> getIdCard(String str) {
		List<String> list = new ArrayList<>();
		// 将给定的正则表达式编译到模式中
		// 创建匹配给定输入与此模式的匹配器。
		Matcher matcher = ID_CARD.matcher(str);
		// 查找字符串中是否有符合的子字符串
		while (matcher.find()) {
			// 查找到符合的即输出
			list.add(matcher.group());
		}
		return list;
	}
	public static void main(String[] args) {
		List<String>	list =RegExpValidatorUtil.addressGet("重庆市沙坪坝区陈家桥街道美丽蓝湾小区9-11-4");
		list.forEach(e->{System.out.println(e);});
	}

}
