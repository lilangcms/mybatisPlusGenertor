package com.lang.prod.tools.regex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.lang.prod.tools.constant.RegexConstant;

public class RegexUtil {
	//正则表达式的字符串
    private String regexStr=new String();

    //正则校验，如果创建对象但是不传入正则表达式而调用该方法就会报错
    public  boolean customMatcher(String testStr){//传入待检测的字符串
        Pattern pattern=Pattern.compile(regexStr);
        Matcher matcher = pattern.matcher(testStr);
        return matcher.matches();
    }
    //正则表达式通用匹配
    private static boolean genericMatcher(String regexExpre,String testStr){
        Pattern pattern=Pattern.compile(regexExpre);
        Matcher matcher = pattern.matcher(testStr);
        return matcher.matches();
    }

    //匹配器
    public static boolean matcher(int RegexConstant_NAME,String testStr){
        boolean flag=false;
        switch (RegexConstant_NAME){
            case RegexConstant.E_MAIL:flag=genericMatcher("\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*",testStr);break;
            case RegexConstant.HEX:flag=genericMatcher("/^#?([a-f0-9]{6}|[a-f0-9]{3})$/",testStr);break;
            case RegexConstant.IP:flag=genericMatcher("/^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/",testStr);break;
            case RegexConstant.HTML_TAG:flag=genericMatcher("/^<([a-z]+)([^<]+)*(?:>(.*)<\\/\\1>|\\s+\\/>)$/",testStr);break;
            case RegexConstant.ZIP_CODE:flag=genericMatcher("/^[u4e00-u9fa5],{0,}$/",testStr);break;
            case RegexConstant.QQ:flag=genericMatcher("[1-9][0-9]{4,}",testStr);break;
        }
        return flag;
    }
    public RegexUtil(String regexStr) {
        this.regexStr = regexStr;
    }

    public String getRegexStr() {
        return regexStr;
    }

    public void setRegexStr(String regexStr) {
        this.regexStr = regexStr;
    }
}
