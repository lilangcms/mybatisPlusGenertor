package com.lang.prod.tools.designPatterns.filter.client;

import com.lang.prod.tools.designPatterns.filter.manager.FilterManager;

public class LangClient {
	FilterManager filterManager;

	public void setFilterManager(FilterManager filterManager) {
		this.filterManager = filterManager;
	}

	public void sendRequest(String request) {
		filterManager.filterRequest(request);
	}
}
