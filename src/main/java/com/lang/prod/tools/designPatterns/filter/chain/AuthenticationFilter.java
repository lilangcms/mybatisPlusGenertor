package com.lang.prod.tools.designPatterns.filter.chain;

import com.lang.prod.tools.designPatterns.filter.LangFilter;

public class AuthenticationFilter implements LangFilter {

	@Override
	public void execute(String request) {
		System.out.println("Authenticating request: " + request);
	}

}
