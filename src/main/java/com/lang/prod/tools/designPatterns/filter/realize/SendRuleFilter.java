package com.lang.prod.tools.designPatterns.filter.realize;

import java.util.List;
import java.util.stream.Collectors;

import com.lang.prod.tools.designPatterns.filter.FilterRule;
import com.lang.prod.tools.designPatterns.filter.RuleScreen;

public class SendRuleFilter implements FilterRule{

	private RuleScreen ruleScreen;
	
	@Override
	public List<Object> filter(List<Object> data) {
		if(ruleScreen==null) {
			return data;
		}
		return data.stream().filter(e->ruleScreen.isRulePassed(e)).collect(Collectors.toList());
	}
	

}
