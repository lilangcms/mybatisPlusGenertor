package com.lang.prod.tools.designPatterns.filter;

public interface LangFilter {
	public void execute(String request);
}
