package com.lang.prod.tools.designPatterns.filter.target;

public class Target {
	public void execute(String request){
        System.out.println("Executing request:"+request);
    }
}
