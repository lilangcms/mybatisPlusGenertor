package com.lang.prod.tools.designPatterns.filter;

public interface RuleScreen {

	/**
	 * 规则过滤
	 * @param data
	 * @return
	 */
	public boolean isRulePassed(Object data);
}
