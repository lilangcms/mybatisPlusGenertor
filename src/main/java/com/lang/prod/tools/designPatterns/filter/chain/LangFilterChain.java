package com.lang.prod.tools.designPatterns.filter.chain;

import java.util.ArrayList;
import java.util.List;

import com.lang.prod.tools.designPatterns.filter.LangFilter;
import com.lang.prod.tools.designPatterns.filter.target.Target;

public class LangFilterChain {
	private List<LangFilter> filterList = new ArrayList<LangFilter>();
	private Target target;

	public void addFilter(LangFilter filter) {
		filterList.add(filter);
	}

	public void execute(String request) {
		for (LangFilter filter : filterList) {
			filter.execute(request);
		}
		target.execute(request);
	}

	public void setTarget(Target target) {
		this.target = target;
	}
}
