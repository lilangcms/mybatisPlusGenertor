package com.lang.prod.tools.designPatterns.filter;

import java.util.List;
/**
 * 过滤器规则
 * @author lilang
 *
 */
public interface FilterRule {
	
	/**
	 * 数据过滤方式
	 * @param data
	 * @return
	 */
	public List<Object>  filter(List<Object> data);

}
