package com.lang.prod.tools.designPatterns.filter.manager;

import com.lang.prod.tools.designPatterns.filter.LangFilter;
import com.lang.prod.tools.designPatterns.filter.chain.LangFilterChain;
import com.lang.prod.tools.designPatterns.filter.target.Target;

public class FilterManager {
	LangFilterChain filterChain;
	 
    public FilterManager(Target target){
        filterChain = new LangFilterChain();
        filterChain.setTarget(target);
    }
    public void setFilter(LangFilter filter){
        filterChain.addFilter(filter);
    }
 
    public void filterRequest(String request){
        filterChain.execute(request);
    }
}
