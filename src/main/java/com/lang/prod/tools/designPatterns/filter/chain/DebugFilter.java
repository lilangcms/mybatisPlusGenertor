package com.lang.prod.tools.designPatterns.filter.chain;

import com.lang.prod.tools.designPatterns.filter.LangFilter;

public class DebugFilter implements LangFilter {

	@Override
	public void execute(String request) {
		System.out.println("request log: " + request);
	}

}
