package com.lang.prod.tools.file;

import java.io.File;
import java.io.FileFilter;

/**
 * 文件名称包含这个关键词
 * @author lilang
 *
 */
public class FileContains implements FileFilter {

	private String keyword;

	public FileContains(String key) {
		this.keyword = key;
	}

	@Override
	public boolean accept(File file) {
		if (file.isDirectory()) {
			return true;
		}
		if (file.getName().toLowerCase().contains(keyword)) {
			return true;
		}
		return false;
	}
	
	

}
