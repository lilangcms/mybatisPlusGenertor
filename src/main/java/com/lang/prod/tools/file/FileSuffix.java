package com.lang.prod.tools.file;

import java.io.File;
import java.io.FileFilter;

/**
 * 文件名称后缀这个关键词
 * @author lilang
 *
 */
public class FileSuffix implements FileFilter {

	private String keyword;

	public FileSuffix(String key) {
		this.keyword = key;
	}

	@Override
	public boolean accept(File file) {
		if (file.isDirectory()) {
			return true;
		}
		if (file.getName().toLowerCase().endsWith(keyword)) {
			return true;
		}
		return false;
	}
	
	

}
