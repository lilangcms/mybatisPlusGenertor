package com.lang.prod.tools.file;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * 文件搜索
 * 
 * @author lilang
 *
 */
public class FileSearchs {

	public static List<File> searchFiles(File folder, final String keyword) {
		List<File> result = new ArrayList<File>();
		if (folder.isFile()) {
			result.add(folder);
		}
		File[] subFolders = folder.listFiles(new FileContains(keyword));
		if (subFolders != null) {
			for (File file : subFolders) {
				if (file.isFile()) {
					// 如果是文件则将文件添加到结果列表中
					result.add(file);
				} else {
					// 如果是文件夹，则递归调用本方法，然后把所有的文件加到结果列表中
					result.addAll(searchFiles(file, keyword));
				}
			}
		}
		return result;
	}
	
	public static List<File> searchContent(File folder, final String keyword) {
		List<File> result = new ArrayList<File>();
		if (folder.isFile()) {
			result.add(folder);
		}
		File[] subFolders = folder.listFiles(new FileContent(keyword));
		if (subFolders != null) {
			for (File file : subFolders) {
				if (file.isFile()) {
					// 如果是文件则将文件添加到结果列表中
					result.add(file);
				} else {
					// 如果是文件夹，则递归调用本方法，然后把所有的文件加到结果列表中
					result.addAll(searchFiles(file, keyword));
				}
			}
		}
		return result;
	}
	
}
