package com.lang.prod.tools.file;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileFilter;
import java.io.FileReader;
import java.io.IOException;

/**
 * 文件名称包含这个关键词
 * @author lilang
 *
 */
public class FileContent implements FileFilter {

	private String keyword;

	public FileContent(String key) {
		this.keyword = key;
	}

	@Override
	public boolean accept(File file) {
		if (file.isDirectory()) {
			return true;
		}
		if(file.canRead()) {
			try {
				FileReader fileReader = new FileReader(file);
				boolean ready = true;
		        BufferedReader bufferedReader = new BufferedReader(fileReader);
		        while(bufferedReader.ready() && ready) {
		        	String str = bufferedReader.readLine();
		        	if(str!=null && str.contains(keyword)) {
		        		ready =  false;
		        	}
		        }
				bufferedReader.close();
				fileReader.close();
				return !ready;
			} catch (IOException e) {
				e.printStackTrace();
				return false;
			}
	       
		}else {
			return false;
		}
	}
	
	

}
