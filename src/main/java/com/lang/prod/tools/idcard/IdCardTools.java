package com.lang.prod.tools.idcard;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class IdCardTools {

	// private final static Pattern pattern =
	// Pattern.compile("(?<!\\d)(?:(?:1[35]\\d{9})|(?:0[1-9]\\d{1,2}-?\\d{7,8}))(?!\\d)");
	private final static Pattern pattern = Pattern.compile(
			"(?<!\\d)^[1-9]\\d{5}(18|19|2([0-9]))\\d{2}(0[0-9]|10|11|12)([0-2][1-9]|30|31)\\d{3}[0-9Xx]$(?!\\d)");

	/**
	 *  * 提取text中的电话和手机号码  * @param text  * @return  
	 */
	public static String pickUp(String text) {
		Matcher matcher = pattern.matcher(text);
		StringBuffer bf = new StringBuffer(64);
		while (matcher.find()) {
			bf.append(matcher.group()).append(",");
		}
		int len = bf.length();
		if (len > 0) {
			bf.deleteCharAt(len - 1);
		}
		return bf.toString();
	}

	public static void main(String[] args) {
		String text = "身份证是50010619860112741X邮箱347465858@qq.com 身份证：3****2199302034**5,  错误的号码13944447771(多了一位)";
		String phones = IdCardTools.pickUp(text);
		System.out.println(phones);
	}

}
