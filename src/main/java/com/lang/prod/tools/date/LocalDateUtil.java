package com.lang.prod.tools.date;

import java.time.DayOfWeek;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.Month;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoField;
import java.time.temporal.ChronoUnit;
import java.util.Date;

public class LocalDateUtil {
	
	
	public void localDate() {
		// 获取当前日期
		LocalDate now = LocalDate.now();
		// 设置日期
		LocalDate localDate = LocalDate.of(2019, 9, 10);
		// 获取年
		int year = localDate.getYear();     //结果：2019
		int year1 = localDate.get(ChronoField.YEAR); //结果：2019
		// 获取月
		Month month = localDate.getMonth();   // 结果：SEPTEMBER
		int month1 = localDate.get(ChronoField.MONTH_OF_YEAR); //结果：9
		// 获取日
		int day = localDate.getDayOfMonth();   //结果：10
		int day1 = localDate.get(ChronoField.DAY_OF_MONTH); // 结果：10
		// 获取星期
		DayOfWeek dayOfWeek = localDate.getDayOfWeek();   //结果：TUESDAY
		int dayOfWeek1 = localDate.get(ChronoField.DAY_OF_WEEK); //结果：2
	}

	public void localTime() {
		// 获取当前时间
		LocalTime now = LocalTime.now();
		// 设置时间
		LocalTime localTime = LocalTime.of(13, 51, 10);
		//获取小时
		int hour = localTime.getHour();    // 结果：13
		int hour1 = localTime.get(ChronoField.HOUR_OF_DAY); // 结果：13
		//获取分
		int minute = localTime.getMinute();  // 结果：51
		int minute1 = localTime.get(ChronoField.MINUTE_OF_HOUR); // 结果：51
		//获取秒
		int second = localTime.getSecond();   // 结果：10
		int second1 = localTime.get(ChronoField.SECOND_OF_MINUTE); // 结果：10
	}
	
	public void localDateTime() {
		LocalTime localTime = LocalTime.of(13, 51, 10);
		LocalDate localDate = LocalDate.of(2019, 9, 10);
		// 获取当前日期时间
		LocalDateTime localDateTime = LocalDateTime.now();
		// 设置日期
		LocalDateTime localDateTime1 = LocalDateTime.of(2019, Month.SEPTEMBER, 10, 14, 46, 56);
		LocalDateTime localDateTime2 = LocalDateTime.of(localDate, localTime);
		LocalDateTime localDateTime3 = localDate.atTime(localTime);
		LocalDateTime localDateTime4 = localTime.atDate(localDate);
		// 获取LocalDate
		LocalDate localDate2 = localDateTime.toLocalDate();
		// 获取LocalTime
		LocalTime localTime2 = localDateTime.toLocalTime();
	}
	
	public void instant() {
		// 创建Instant对象
		Instant instant = Instant.now();
		// 获取秒
		long currentSecond = instant.getEpochSecond();
		// 获取毫秒
		long currentMilli = instant.toEpochMilli();
	}
	
	public void editTime() {
		// 创建日期：2019-09-10 14:46:56
		LocalDateTime localDateTime = LocalDateTime.of(2019, Month.SEPTEMBER, 10, 14, 46, 56);
		//增加一年
		localDateTime = localDateTime.plusYears(1);  //结果： 2020-09-10 14:46:56
		localDateTime = localDateTime.plus(1, ChronoUnit.YEARS); //结果： 2021-09-10 14:46:56
		//减少一个月
		localDateTime = localDateTime.minusMonths(1);  //结果： 2021-08-10 14:46:56
		localDateTime = localDateTime.minus(1, ChronoUnit.MONTHS); //结果： 2021-07-10 14:46:56
	}
	
	/**
	  * LocalDateTime转毫秒时间戳
	  * @param localDateTime LocalDateTime
	  * @return 时间戳
	  */
	public static Long localDateTimeToTimestamp(LocalDateTime localDateTime) {
	    try {
	        ZoneId zoneId = ZoneId.systemDefault();
	        Instant instant = localDateTime.atZone(zoneId).toInstant();
	        return instant.toEpochMilli();
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
	    return null;
	}

	/**
	 * 时间戳转LocalDateTime
	 * @param timestamp 时间戳
	 * @return LocalDateTime
	 */
	public static LocalDateTime timestampToLocalDateTime(long timestamp) {
	    try {
	        Instant instant = Instant.ofEpochMilli(timestamp);
	        ZoneId zone = ZoneId.systemDefault();
	        return LocalDateTime.ofInstant(instant, zone);
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
	    return null;
	}

	/**
	 * Date转LocalDateTime
	 * @param date Date
	 * @return LocalDateTime
	 */
	public static LocalDateTime dateToLocalDateTime(Date date) {
	    try {
	        Instant instant = date.toInstant();
	        ZoneId zoneId = ZoneId.systemDefault();
	        return instant.atZone(zoneId).toLocalDateTime();
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
	    return null;
	}

	/**
	 * LocalDateTime转Date
	 * @param localDateTime LocalDateTime
	 * @return Date
	 */
	public static Date localDateTimeToDate(LocalDateTime localDateTime) {
	    try {
	        ZoneId zoneId = ZoneId.systemDefault();
	        ZonedDateTime zdt = localDateTime.atZone(zoneId);
	        return Date.from(zdt.toInstant());
	    } catch (Exception e) {
	        e.printStackTrace();
	    }
	    return null;
	}
}
