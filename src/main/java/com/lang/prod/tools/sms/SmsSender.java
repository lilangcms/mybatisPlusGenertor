package com.lang.prod.tools.sms;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsRequest;
import com.aliyuncs.dysmsapi.model.v20170525.SendSmsResponse;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;
import com.aliyuncs.profile.IClientProfile;
import com.lang.prod.config.AliyuncsConfig;
/**
 * 阿里云短信发送
 * @author lilang
 *
 */
public class SmsSender
{
  private static final Logger log = LoggerFactory.getLogger(SmsSender.class);

  public static boolean sendMessage(AliyuncsConfig config, String number, String text)
  {
    if ((StringUtils.isBlank(config.getAccessKeyId())) || (StringUtils.isBlank(config.getAccessKeySecret()))) {
      return false;
    }
    try
    {
      System.setProperty("sun.net.client.defaultConnectTimeout", "10000");
      System.setProperty("sun.net.client.defaultReadTimeout", "10000");

      String accessKeyId = config.getAccessKeyId();
      String accessKeySecret = config.getAccessKeySecret();

      IClientProfile profile = DefaultProfile.getProfile("cn-hangzhou", accessKeyId, accessKeySecret);
      DefaultProfile.addEndpoint("cn-hangzhou", "cn-hangzhou", "Dysmsapi", "dysmsapi.aliyuncs.com");
      IAcsClient acsClient = new DefaultAcsClient(profile);

      SendSmsRequest request = new SendSmsRequest();

      request.setMethod(MethodType.POST);

      request.setPhoneNumbers(number);

      request.setSignName(config.getSignName());

      request.setTemplateCode(config.getTemplateCode());

      request.setTemplateParam("{\"code\":\"" + text + "\"}");

      request.setOutId("yourOutId");

      SendSmsResponse sendSmsResponse = (SendSmsResponse)acsClient.getAcsResponse(request);

      return ((sendSmsResponse.getCode() != null) && (sendSmsResponse.getCode().equals("OK")));
    }
    catch (ClientException e)
    {
      log.error("短信发送失败:", e); }
    return false;
  }
}