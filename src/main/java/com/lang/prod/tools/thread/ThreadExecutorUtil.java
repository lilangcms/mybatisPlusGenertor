package com.lang.prod.tools.thread;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * 线程工具
 * 
 * @author lilang
 *
 *         ①newSingleThreadExecutor 单个线程的线程池，即线程池中每次只有一个线程工作，单线程串行执行任务
 *         ②newFixedThreadExecutor(n)
 *         固定数量的线程池，没提交一个任务就是一个线程，直到达到线程池的最大数量，然后后面进入等待队列直到前面的任务完成才继续执行
 *         ③newCacheThreadExecutor（推荐使用）
 *         可缓存线程池，当线程池大小超过了处理任务所需的线程，那么就会回收部分空闲（一般是60秒无执行）的线程，当有任务来时，又智能的添加新线程来执行。
 *         ④newScheduleThreadExecutor 大小无限制的线程池，支持定时和周期性的执行线程
 */
public class ThreadExecutorUtil {
	
public static Integer FIXE_MAX_POOL = 5;
	/**
	 * new ThreadPoolExecutor(nThreads, nThreads,
                                      0L, TimeUnit.MILLISECONDS,
                                      new LinkedBlockingQueue<Runnable>())
	 */
	private static ExecutorService fixedThreadPool = Executors.newFixedThreadPool(FIXE_MAX_POOL);
	/**
	 * corePoolSize：核心线程数量，会一直存在，除非allowCoreThreadTimeOut设置为true
	 * maximumPoolSize：线程池允许的最大线程池数量
	 * keepAliveTime：线程数量超过corePoolSize，空闲线程的最大超时时间
	 * unit：超时时间的单位
	 * workQueue：工作队列，保存未执行的Runnable 任务
	 * threadFactory：创建线程的工厂类
	 * handler：当线程已满，工作队列也满了的时候，会被调用。被用来实现各种拒绝策略。
	 */
	public static ExecutorService customFixedThreadPool = new ThreadPoolExecutor(1, 1, 0L, TimeUnit.MILLISECONDS, new LinkedBlockingQueue<Runnable>());
	
	private static ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);
	
	public static void execute(Thread thread) {
		fixedThreadPool.execute(thread);
	}
	
	public static void execute(Runnable runnable) {
		fixedThreadPool.execute(runnable);
	}
	/**
	  *  延迟10秒执行
	 * lang.li
	 * @param runnable
	 * 2019年1月3日下午4:13:55
	 */
	public static void schedule(Runnable runnable) {
		executor.schedule(runnable, 10, TimeUnit.SECONDS);
	}
	
	
	/**
	 * 延迟多少秒执行
	 * lang.li
	 * @param runnable
	 * @param seconds
	 * 2019年1月16日上午10:16:59
	 */
	public static void schedule(Runnable runnable,Integer seconds) {
		executor.schedule(runnable, seconds, TimeUnit.SECONDS);
	}

}
