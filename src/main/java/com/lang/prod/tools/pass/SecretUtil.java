package com.lang.prod.tools.pass;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import org.springframework.util.DigestUtils;

import java.nio.charset.StandardCharsets;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.Base64;

public class SecretUtil {

	/**
	 * 加密
	 * @param key
	 * @param ivStr
	 * @param data
	 * @return
	 * @throws NoSuchPaddingException
	 * @throws NoSuchAlgorithmException
	 * @throws InvalidAlgorithmParameterException
	 * @throws InvalidKeyException
	 * @throws BadPaddingException
	 * @throws IllegalBlockSizeException
	 */
    public static String encrypt(String key, String ivStr, String data) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidAlgorithmParameterException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
        //获取秘钥字符串的byte数组 
        byte[] raw = key.getBytes(StandardCharsets.US_ASCII);
        //获取key实例 
        SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");
        //获取加密算法实例 
        Cipher cipher = Cipher.getInstance("AES/CBC/NoPadding");
        //获取初始向量实例 
        IvParameterSpec iv = new IvParameterSpec(ivStr.getBytes());
        //初始化加密算法实例 
        cipher.init(Cipher.ENCRYPT_MODE, skeySpec, iv);
        //获取明文数据byte数组 
        byte[] dataBytes = data.getBytes();
        //获取byte数组长度用于判断是否为16倍数 
        int length = dataBytes.length;
        int appendLength = dataBytes.length % 16;
        if (appendLength != 0) {
            //构造新数组,长度为补齐到的16倍数 
            byte[] appendBytes = new byte[length + 16 - appendLength];
            //拷贝旧数组内容到前段 
            System.arraycopy(dataBytes, 0, appendBytes, 0, length);
            //构造填充内容 
            byte b = 32;
            //将填充内容填充至新数组的剩余位置 
            Arrays.fill(appendBytes, length, appendBytes.length, b);
            //赋值给待加密byte数组 
            dataBytes = appendBytes;
        }
        //加密 
        byte[] bytes = cipher.doFinal(dataBytes);
        //将加密得到的byte数组编码为Base64两次 
        Base64.Encoder encoder = Base64.getEncoder();
        byte[] encode = encoder.encode(bytes);
        return encoder.encodeToString(encode);
    }

    /**
     * 解密
     * @param key
     * @param ivStr
     * @param data
     * @return
     * @throws NoSuchPaddingException
     * @throws NoSuchAlgorithmException
     * @throws InvalidAlgorithmParameterException
     * @throws InvalidKeyException
     * @throws BadPaddingException
     * @throws IllegalBlockSizeException
     */
    public static String getData(String key,String ivStr,String data) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidAlgorithmParameterException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException {
        Base64.Decoder decoder = Base64.getDecoder();
        byte[] bytes = decoder.decode(data.getBytes(StandardCharsets.ISO_8859_1));
        bytes = decoder.decode(bytes);
        Cipher cipher = Cipher.getInstance("AES/CBC/NoPadding");
        byte[] raw = key.getBytes(StandardCharsets.US_ASCII);
        //获取key实例 
        SecretKeySpec skeySpec = new SecretKeySpec(raw, "AES");
        IvParameterSpec iv = new IvParameterSpec(ivStr.getBytes());
        //初始化加密算法实例 
        cipher.init(Cipher.DECRYPT_MODE, skeySpec, iv);
        return new String(cipher.doFinal(bytes)).trim();
    }

    
    public static void main(String[] args) throws InvalidKeyException, NoSuchPaddingException, NoSuchAlgorithmException, InvalidAlgorithmParameterException, BadPaddingException, IllegalBlockSizeException {
		String key = "813a4c46ef95c01073eaf4746c914bf4";
		String ivStr ="1234567890123456";
		String params = "[{\"caseTime\":1618906316038,\"caseType\":5,\"city\":\"重庆市\",\"county\":\"南岸区\",\"id\":\"1\",\"idCard\":\"511521198705112886\",\"level\":1,\"money\":6666.66,\"name\":\"张三\",\"number\":\"15883104999\",\"numberType\":1,\"province\":\"重庆\",\"reason\":\"测试天彦通信预警接收\",\"sendTime\":1618906316038,\"times\":6,\"vendorSource\":\"测试\",\"vendorType\":666,\"website\":\"http://www.zhajinhua.com\"}]";
		System.out.println("加密前:"+params);
		String ds = encrypt(key, ivStr, params);
		//String ds="bEw1VERyRVE4cEVvLzZiVkZRZ2dtWlovRFBlc0ZEZ3RyZldkejFTQStCYmJJK3hSbytuR2tSUURhdHpnbTNyTi9VZDRMdDhldVhrU1VhZldLdzZPeDZsTnJ2SFMyd3pkTTdDVkFsVGxOdkFiSDh6WkEySlFWeHBEWitkV1pYSTRyT24rMkJRTEdlRVVKUS9GemJxZFRWWHVFSUR0VWZMbE1SaWYwaDlOajRDV3N6TDZaTXJsempYcFlUR0hodnBMNUIzbFA4MCtpZ2k1ak9FSmpDZWdMUFAxdnoyU2dhcVd0bDZGVnF5MGlZYXAybjEzd3Y0ZWpmdXRFSy9WeXk3bGNVYm94VU9lSlZlNlZzT0Y2dGdIVzFCMFR4dWxGa1hkVzN0OU0rcEJqZk5oOXVYU1NGM0Fwd0FwYXZLUWxxUDZYdHhIVzB2bUpVN2NQUENZZDAvUm9oN2l3RGV0ZmJLY0h0ZUcxWXF4Q2F1a2luZnZhS1NjMTMrb2NNMEVJcmYxOU04N0VkMkllSHN1c1lrd0lDMXVrZEM1VzNDRlgraHFWR0tLZFltbEZGUkxGU0srWUxReCt1b0I4aUJwMzZITE8ra003YjdwK0FWZTVSdXVPbHpjNUM5RjVGZzVjL3BzTXNObysxNWtvZUdEQmZtT3BKUjNwSkU2bGk3clpzd2JKQW9ncHdrcDlrZE5BTEs4eHhTRXVvQlYvOXRzZlJ5dFMzTXByeWcrUVNQeE9TbU1BNmxqRW1MUnhZWU5TN3Ntcmtvd0JhdjN0T2JodmkvZVY5aUdWV213Vld4UjNoWGQwb0I2b2pYOCsrMD0";
		System.out.println("加密后:"+ds);
		String newDs = getData(key, ivStr, ds);
		System.out.println("解密:"+newDs);
	}



}
