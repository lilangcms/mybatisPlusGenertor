package com.skycomm.fraud.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author lilang
 * @since 2021-04-12
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("fraud_monitor")
@ApiModel(value="FraudMonitor对象", description="")
public class FraudMonitor implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "ID")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty(value = "创建时间")
    @TableField("create_time")
    private LocalDateTime createTime;

    @ApiModelProperty(value = "标题")
    @TableField("title")
    private String title;

    @ApiModelProperty(value = "消息描述")
    @TableField("message")
    private String message;

    @ApiModelProperty(value = "执行SQL")
    @TableField("sql")
    private String sql;

    @ApiModelProperty(value = "参数")
    @TableField("parameter")
    private String parameter;

    @ApiModelProperty(value = "阀值")
    @TableField("threshold")
    private Integer threshold;

    @ApiModelProperty(value = "状态 1 有效 0 无效")
    @TableField("state")
    private Integer state;

    @ApiModelProperty(value = "预警发送号码")
    @TableField("warn_number")
    private String warnNumber;

    @ApiModelProperty(value = "最后执行时间")
    @TableField("send_time")
    private LocalDateTime sendTime;

    @ApiModelProperty(value = "执行结果")
    @TableField("result")
    private String result;


    public static final String ID = "id";

    public static final String CREATE_TIME = "create_time";

    public static final String TITLE = "title";

    public static final String MESSAGE = "message";

    public static final String SQL = "sql";

    public static final String PARAMETER = "parameter";

    public static final String THRESHOLD = "threshold";

    public static final String STATE = "state";

    public static final String WARN_NUMBER = "warn_number";

    public static final String SEND_TIME = "send_time";

    public static final String RESULT = "result";

}
