package com.skycomm.fraud.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import java.time.LocalDateTime;
import com.baomidou.mybatisplus.annotation.TableField;
import java.io.Serializable;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 
 * </p>
 *
 * @author lilang
 * @since 2020-09-25
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("config")
@ApiModel(value="Config对象", description="")
public class Config implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @TableField("create_dt")
    private LocalDateTime createDt;

    @TableField("group_name")
    private String groupName;

    @TableField("cfg_key")
    private String cfgKey;

    @TableField("cfg_value")
    private String cfgValue;

    @TableField("num")
    private Integer num;

    @TableField("remarks")
    private String remarks;


    public static final String ID = "id";

    public static final String CREATE_DT = "create_dt";

    public static final String GROUP_NAME = "group_name";

    public static final String CFG_KEY = "cfg_key";

    public static final String CFG_VALUE = "cfg_value";

    public static final String NUM = "num";

    public static final String REMARKS = "remarks";

}
