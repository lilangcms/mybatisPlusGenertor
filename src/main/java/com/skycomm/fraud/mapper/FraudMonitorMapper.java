package com.skycomm.fraud.mapper;

import com.skycomm.fraud.entity.FraudMonitor;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lilang
 * @since 2021-04-12
 */
public interface FraudMonitorMapper extends BaseMapper<FraudMonitor> {

}
