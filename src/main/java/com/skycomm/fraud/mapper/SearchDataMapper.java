package com.skycomm.fraud.mapper;

import com.skycomm.fraud.entity.SearchData;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lilang
 * @since 2020-09-25
 */
public interface SearchDataMapper extends BaseMapper<SearchData> {

}
