package com.skycomm.fraud.mapper;

import com.skycomm.fraud.entity.Config;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author lilang
 * @since 2020-09-25
 */
public interface ConfigMapper extends BaseMapper<Config> {

}
