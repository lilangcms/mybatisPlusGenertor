package com.skycomm.fraud.service.impl;

import com.skycomm.fraud.entity.SearchData;
import com.skycomm.fraud.mapper.SearchDataMapper;
import com.skycomm.fraud.service.SearchDataService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lilang
 * @since 2020-09-25
 */
@Service
public class SearchDataServiceImpl extends ServiceImpl<SearchDataMapper, SearchData> implements SearchDataService {

}
