package com.skycomm.fraud.service.impl;

import com.skycomm.fraud.entity.FraudMonitor;
import com.skycomm.fraud.mapper.FraudMonitorMapper;
import com.skycomm.fraud.service.FraudMonitorService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lilang
 * @since 2021-04-12
 */
@Service
public class FraudMonitorServiceImpl extends ServiceImpl<FraudMonitorMapper, FraudMonitor> implements FraudMonitorService {

}
