package com.skycomm.fraud.service.impl;

import com.skycomm.fraud.entity.Config;
import com.skycomm.fraud.mapper.ConfigMapper;
import com.skycomm.fraud.service.ConfigService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author lilang
 * @since 2020-09-25
 */
@Service
public class ConfigServiceImpl extends ServiceImpl<ConfigMapper, Config> implements ConfigService {

}
