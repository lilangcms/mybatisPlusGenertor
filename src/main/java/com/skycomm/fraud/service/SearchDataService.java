package com.skycomm.fraud.service;

import com.skycomm.fraud.entity.SearchData;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author lilang
 * @since 2020-09-25
 */
public interface SearchDataService extends IService<SearchData> {

}
