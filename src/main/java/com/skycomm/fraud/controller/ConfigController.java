package com.skycomm.fraud.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author lilang
 * @since 2020-09-25
 */
@RestController
@RequestMapping("/config")
public class ConfigController {

}

